export class FlagEntry {
    #last_used: number = Date.now();

    constructor(readonly flag: number, readonly provider?: string) {}

    get last_used(): number {
        return this.#last_used;
    }

    isOlder(ttl: number): boolean {
        const now = Date.now();
        return (now - this.#last_used) / 1000 > ttl;
    }

    touch(): void {
        this.#last_used = Date.now();
    }
}

export class Cache {
    #cache: Map<string, FlagEntry> = new Map();

    constructor(readonly ttl: number = 300, readonly max: number = 100) {}

    /**
     * Get the flags for a given domain.
     * Auto-removes the domain entry if it has expired.
     * @param domain domain to check if present in the cache
     * @returns the flag if the domain is found and not expired
     */
    getFlag(domain: string): FlagEntry | undefined {
        const entry = this.#cache.get(domain);
        if (entry && !entry.isOlder(this.ttl)) {
            entry.touch();
            return entry;
        }
        this.#cache.delete(domain);
        return;
    }

    /**
     * Add a new entry to the cache
     */
    setFlag(domain: string, flag: FlagEntry): void {
        if (this.#cache.size === this.max) this.#pruneCache();
        this.#cache.set(domain, flag);
    }

    #pruneCache(): void {
        let leastUsed = Date.now();
        let leastUsedKey = '';
        this.#cache.forEach((v, k) => {
            if (v.last_used < leastUsed) {
                leastUsed = v.last_used;
                leastUsedKey = k;
            }
            // remove also the expired entries
            if (v.isOlder(this.ttl)) this.#cache.delete(k);
        });
        this.#cache.delete(leastUsedKey);
    }
}
