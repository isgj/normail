/* eslint-disable max-len */
import dns from 'dns/promises';
import { createNormalizer, normalize } from '.';

jest.mock('dns/promises', () => ({
    resolveMx: jest.fn(),
}));

beforeEach(() => {
    (dns.resolveMx as jest.Mock).mockReset();
});

describe('createNormalizer', () => {
    it('returns normalizer that will use cache', async () => {
        const n = createNormalizer();
        (dns.resolveMx as jest.Mock).mockResolvedValueOnce([
            { exchange: 'blabla.email.com' },
        ]);
        await n('email@email.com');
        await n('email@email.com');
        expect(dns.resolveMx).toHaveBeenCalledTimes(1);
        expect((dns.resolveMx as jest.Mock).mock.calls[0][0]).toBe('email.com');
    });
});

describe('normalize', () => {
    it('should return the error and the original email', async () => {
        (dns.resolveMx as jest.Mock).mockRejectedValueOnce(
            new Error('internet down'),
        );
        const result = await normalize('email@email.com');
        expect(result.original).toBe(result.normalized);
        expect(result.provider).toBeUndefined();
        expect(result.error).toBeDefined();
        expect(result.error?.message).toBe('internet down');
    });

    it.each([
        [
            'google plus',
            'gmail+d+rule@gmail.com',
            'mx.google.com',
            'gmail@gmail.com',
            'google.com',
        ],
        [
            'google period',
            'gma.il@gmail.com',
            'mx.google.com',
            'gmail@gmail.com',
            'google.com',
        ],
        [
            'google plus and period',
            'gMA.il+d+rule@gmail.com',
            'mx.google.com',
            'gmail@gmail.com',
            'google.com',
        ],
        [
            'apple plus',
            'a+aa@apple.com',
            'mx.icloud.com',
            'a@apple.com',
            'icloud.com',
        ],
        [
            'fastmail local',
            'fa+sd@asd.fast.com',
            'mx.messagingengine.com',
            'asd@fast.com',
            'messagingengine.com',
        ],
        [
            'fastmail plus',
            'fa+sd@fast.com',
            'mx.messagingengine.com',
            'fa@fast.com',
            'messagingengine.com',
        ],
        [
            'protonmain plus',
            'pro+ton@pm.me',
            'mx.protonmail.ch',
            'pro@pm.me',
            'protonmail.ch',
        ],
        [
            'outlook case sensitive and plus',
            'AaAa+aaa@ms.com',
            'mx.outlook.com',
            'AaAa@ms.com',
            'outlook.com',
        ],
        [
            'yahoo dash and period',
            'ya.h.-s-s@yahoo.com',
            'mx.yahoodns.net',
            'yah@yahoo.com',
            'yahoodns.net',
        ],
    ])(
        'should apply %s behaviour',
        async (_, original, mock, normal, provider) => {
            (dns.resolveMx as jest.Mock).mockResolvedValueOnce([
                { exchange: mock },
            ]);
            const result = await normalize(original);
            expect(result.original).toBe(original);
            expect(result.normalized).toBe(normal);
            expect(result.provider).toBe(provider);
        },
    );

    it('should pass through the email', async () => {
        (dns.resolveMx as jest.Mock).mockResolvedValueOnce({
            exchange: 'google.com',
        });
        const result = await normalize('e.m.ail+mail@gmail.com', {
            passThroughDomains: ['gmail.com'],
        });
        expect(result.original).toBe(result.normalized);
        expect(result.provider).toBeUndefined();
        expect(result.error).toBeUndefined();
        expect(dns.resolveMx).toHaveBeenCalledTimes(0);
    });
});
