import dns from 'dns/promises';
import { providerFlags, Flag } from './flags';
import { Cache, FlagEntry } from './cache';

interface CacheOpts {
    /**
     * Maximum time(in seconds) to cache mx records for a given domain
     */
    ttl?: number;
    /**
     * Maximum entries to keep in the cache
     */
    max?: number;
}

interface NormOpts {
    /**
     * Emails that have a domain (whatever comes after `@`) present in the list
     * will pass through without being changed
     */
    passThroughDomains?: string[];
}

/**
 * Will return a normalizer function that will cache result.
 */
export function createNormalizer(
    opts: CacheOpts & NormOpts = {},
): (email: string, opt?: NormOpts) => Promise<NormalizedEmail> {
    opts = { ttl: 300, max: 100, ...opts };
    const cache = new Cache(opts.ttl, opts.max);
    return async function (email: string, opt?: NormOpts) {
        return await normalizeEmail(email, cache, { ...opts, ...opt });
    };
}

/**
 * Will try to normalize the email
 * @param email email to normalize
 */
export async function normalize(
    email: string,
    opts?: NormOpts,
): Promise<NormalizedEmail> {
    return await normalizeEmail(email, undefined, opts);
}

class NormalizedEmail {
    constructor(
        readonly original: string,
        readonly normalized: string,
        readonly provider?: string,
        readonly error?: Error,
    ) {}
}

const flagTransforms: Record<number, (l: string) => string> = {
    [Flag.PERIOD]: (l: string) => l.replace(/\./g, ''),
    [Flag.PLUS_ADDR]: (l: string) => l.split('+')[0],
    [Flag.DASH_ADDR]: (l: string) => l.split('-')[0],
};

async function normalizeEmail(email: string, cache?: Cache, opts?: NormOpts) {
    opts = {
        passThroughDomains: [],
        ...opts,
    };
    let [local, domain] = email.trim().split('@');

    if (opts.passThroughDomains?.includes(domain)) {
        return new NormalizedEmail(email, email);
    }

    let flagEntry = cache?.getFlag(domain);
    // expired, not in cache or no cache at all
    if (flagEntry === undefined) {
        try {
            flagEntry = await getFlag(domain, cache);
        } catch (error) {
            return new NormalizedEmail(email, email, undefined, error);
        }
    }
    const domainFlag = flagEntry.flag;
    if (domainFlag & Flag.LOCAL_AS_HOSTNAME) {
        const domainParts = domain.split('.');
        if (domainParts.length > 2) {
            local = domainParts[0];
            domain = domainParts.slice(1).join('.');
        }
    }

    [Flag.PLUS_ADDR, Flag.DASH_ADDR, Flag.PERIOD].forEach((f) => {
        if (domainFlag & f) {
            local = flagTransforms[f](local);
        }
    });
    if (!(domainFlag & Flag.CASE_SENSITIVE)) {
        local = local.toLowerCase();
        domain = domain.toLowerCase();
    }

    return new NormalizedEmail(email, `${local}@${domain}`, flagEntry.provider);
}

async function getFlag(domain: string, cache?: Cache) {
    const entries = await dns.resolveMx(domain);
    const provider = Object.keys(providerFlags).find((domain) =>
        entries.some((mx) => mx.exchange.endsWith(domain)),
    );
    const flag = provider ? providerFlags[provider] ?? 0 : 0;
    const flagEntry = new FlagEntry(flag, provider);
    if (cache) cache.setFlag(domain, flagEntry);
    return flagEntry;
}
