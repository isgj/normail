import { FlagEntry, Cache } from './cache';

describe('FlagEntry', () => {
    it('should check is older correctly', () => {
        const now = Date.now();
        // Created 5 seconds before
        jest.spyOn(Date, 'now').mockImplementationOnce(() => now - 5000);
        const flag = new FlagEntry(0);

        expect(flag.isOlder(4)).toBe(true);
        expect(flag.isOlder(6)).toBe(false);
    });

    it('should update last used', () => {
        const now = Date.now();
        // Created 5 seconds before
        jest.spyOn(Date, 'now').mockImplementationOnce(() => now - 5000);
        const flag = new FlagEntry(0);

        expect(flag.isOlder(2)).toBe(true);

        flag.touch();
        // Now is not older than 1 second
        expect(flag.isOlder(1)).toBe(false);
    });
});

const sleep = (n: number) => new Promise((resolve) => setTimeout(resolve, n));

describe('Cache', () => {
    let cache: Cache;

    beforeEach(() => {
        cache = new Cache(5, 5);
    });

    it('should return not expired flag from cache', () => {
        const flag = new FlagEntry(0);
        cache.setFlag('domain', flag);

        expect(cache.getFlag('domain')).toStrictEqual(flag);
    });

    it('should not return expired flag from cache', () => {
        const now = Date.now();
        // Created 6 seconds before, cache has 5 seconds ttl
        jest.spyOn(Date, 'now').mockImplementationOnce(() => now - 6000);
        const flag = new FlagEntry(0);
        cache.setFlag('domain', flag);

        expect(cache.getFlag('domain')).toBeUndefined();
    });

    it('should use old entry making it newer than the others', async () => {
        const now = Date.now();
        // Created 2 seconds before
        jest.spyOn(Date, 'now').mockImplementationOnce(() => now - 2000);
        const old_flag = new FlagEntry(0);
        cache.setFlag('old_flag', old_flag);

        // add newer flags
        Array(4)
            .fill(0)
            .forEach((f, i) => cache.setFlag(`flag_${i}`, new FlagEntry(f)));

        // make sure some time is passing
        await sleep(2);
        // now cache is full, old flag is still there
        // by getting it(using) now it's newer
        expect(cache.getFlag('old_flag')).toStrictEqual(old_flag);
        // make sure some time is passing
        await sleep(2);

        // add newer flags
        Array(4)
            .fill(0)
            .forEach((f, i) => cache.setFlag(`flag_${i}`, new FlagEntry(f)));
        // old flag gone
        expect(cache.getFlag('old_flag')).toStrictEqual(old_flag);
    });

    it('should remove least used entry when limit is reached', () => {
        const now = Date.now();
        // Created 2 seconds before
        jest.spyOn(Date, 'now').mockImplementationOnce(() => now - 2000);
        const old_flag = new FlagEntry(0);
        cache.setFlag('old_flag', old_flag);

        // add newer flags
        Array(4)
            .fill(0)
            .forEach((f, i) => cache.setFlag(`flag_${i}`, new FlagEntry(f)));

        // now cache is full, old flag is still there
        // add another flag
        cache.setFlag('new_flag', new FlagEntry(1));
        // old flag gone
        expect(cache.getFlag('old_flag')).toBeUndefined();
    });
});
