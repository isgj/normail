export enum Flag {
    PLUS_ADDR = 1,
    PERIOD = 1 << 1,
    DASH_ADDR = 1 << 2,
    CASE_SENSITIVE = 1 << 3,
    LOCAL_AS_HOSTNAME = 1 << 4,
}

export const providerFlags: Record<string, Flag> = {
    'icloud.com': Flag.PLUS_ADDR,
    'messagingengine.com': Flag.PLUS_ADDR | Flag.LOCAL_AS_HOSTNAME,
    'outlook.com': Flag.PLUS_ADDR | Flag.CASE_SENSITIVE,
    'protonmail.ch': Flag.PLUS_ADDR,
    'mx.yandex.net': Flag.PLUS_ADDR,
    'yandex.ru': Flag.PLUS_ADDR,
    'zoho.com': Flag.PLUS_ADDR,
    'emailsrvr.com': Flag.PLUS_ADDR,
    'yahoodns.net': Flag.DASH_ADDR | Flag.PERIOD,
    'google.com': Flag.PLUS_ADDR | Flag.PERIOD,
};
