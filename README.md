![ci pipeline](https://gitlab.com/isgj/normail/badges/main/pipeline.svg)
# normail

Light library that tries to normalize emails

### Usage

As a sipmple function

```ts
import { mormalize } from 'normail';

const normalized = await normalize('Some.email+extra@gmail.com')
// Output
// NormalizedEmail {
//     original: 'Some.email+extra@gmail.com',
//     normalized: 'someemail@gmail.com',
//     provider: 'google.com',
//     error: undefined
// }
```

Or you can create a new function that will cache the results
```ts
import { createNormalizer } from 'normail';

// create the normalizer only once
const normalize = createNormalizer({
    ttl: 300, // cache the results for 300 seconds
    max: 100  // max entries to keep on cache
})

// used it more times
const normalized = await normalize('Some.email+extra@gmail.com')
// Output as before
```

---

Different providers have different rules, for what this library supports you can check the [provider rules](https://gitlab.com/isgj/normail/-/blob/main/src/flags.ts)

---

### Credits

The rules were copied from https://github.com/gmr/email-normalize
